#ifndef __MQTT_CONFIG__
#define __MQTT_CONFIG__

#include <JsonStruct.h>

#define JSON_MQTT_CLIENT_NAME "mqttClientName"
#define JSON_MQTT_BROKER_HOST "mqttBrokerHost"
#define JSON_MQTT_BROKER_PORT "mqttBrokerPort"
#define JSON_MQTT_TOPIC_ROOT "mqttRootTopic"
#define JSON_MQTT_USER "mqttUser"
#define JSON_MQTT_PASS "mqttPass"

struct MqttConfig
{
    const char *clientName;
    const char *brokerHost;
    int brokerPort;
    const char *topicRoot;
    const char *user;
    const char *pass;
};

struct MqttConfigJson : public MqttConfig, public JsonStruct
{
    void mapJsonObject(JsonObject &root)
    {
        root[JSON_MQTT_CLIENT_NAME] = clientName;
        root[JSON_MQTT_BROKER_HOST] = brokerHost;
        root[JSON_MQTT_BROKER_PORT] = brokerPort;
        root[JSON_MQTT_TOPIC_ROOT] = topicRoot;
        root[JSON_MQTT_USER] = user;
        root[JSON_MQTT_PASS] = pass;
    }
    void fromJsonObject(JsonObject &json)
    {
        if (!verifyJsonObject(json))
        {
            Serial.println("ERROR: cannot parse JSON object");
            valid = 0;
            return;
        }
        clientName = getAttr(json, JSON_MQTT_CLIENT_NAME);
        brokerHost = getAttr(json, JSON_MQTT_BROKER_HOST);
        brokerPort = getIntAttrFromJson(json, JSON_MQTT_BROKER_PORT);
        topicRoot = getAttr(json, JSON_MQTT_TOPIC_ROOT);
        user = getAttr(json, JSON_MQTT_USER);
        pass = getAttr(json, JSON_MQTT_PASS);
        valid = 1;
    };
};

#endif