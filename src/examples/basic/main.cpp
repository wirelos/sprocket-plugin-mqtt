#include "config.h"
#include "WiFiNet.h"
#include "Sprocket.h"
#include "MqttPlugin.h"

WiFiNet *network;
Sprocket *sprocket;

Task publishHeap;

void setup()
{
    publishHeap.set(TASK_SECOND * 5, TASK_FOREVER, [](){
        // publish locally to local/topic
        // and remotely to wirelos/mqttSprocket-out/local/topic
        sprocket->publish("local/topic", "heap=" + String(ESP.getFreeHeap()));
    });
    
    sprocket = new Sprocket(
        {STARTUP_DELAY, SERIAL_BAUD_RATE});
    
    // subscribe locally to local/topic
    // and remotely to wirelos/mqttSprocket-in/local/topic
    sprocket->subscribe("local/topic", [](String msg){
        PRINT_MSG(Serial, "LOCAL", msg.c_str());
    });
    sprocket->addTask(publishHeap);
    sprocket->addPlugin(
        new MqttPlugin({"clientId", "192.168.1.2", 1883, "some/topic", "myuser", "mypass"}));

    network = new WiFiNet(
        SPROCKET_MODE,
        STATION_SSID,
        STATION_PASSWORD,
        AP_SSID,
        AP_PASSWORD,
        HOSTNAME,
        CONNECT_TIMEOUT);
    network->connect();

    sprocket->activate();
}

void loop()
{
    sprocket->loop();
    yield();
}